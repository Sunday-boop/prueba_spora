package com.example.prueba_spora

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_usuario.*
import kotlinx.android.synthetic.main.item_usuario.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UsuarioActvity : AppCompatActivity() {
    private lateinit var database: AppDatabase
    private lateinit var usuario: Usuario
    private lateinit var usuarioLiveDAta: LiveData<Usuario>
    private val EDIT_ACTIVITY = 49

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_usuario)

        database = AppDatabase.getDatabase(this)

        val idUsuario = intent.getIntExtra("id", 0)

        val imageUri = ImagenController.getImageUri(this, idUsuario.toLong())
        imagen_det.setImageURI(imageUri)
        usuarioLiveDAta = database.usuarios().get(idUsuario)
        usuarioLiveDAta.observe(this, Observer {
            usuario = it

            nombre_det.text = usuario.nombre
            correo_det.text = usuario.correo
            telefono_det.text = usuario.telefono
            edad_det.text = usuario.edad.toString()
        })

        btn_regresar_mos.setOnClickListener {
            startActivity(Intent(this, MostrarUsuariosActivity::class.java))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.usuario_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.editar_usu ->{
                val intent = Intent(this, NuevoUsuarioActivity::class.java)
                intent.putExtra("usuario", usuario)
                startActivityForResult(intent, EDIT_ACTIVITY)
            }

            R.id.eliminar_usu->{
                usuarioLiveDAta.removeObservers(this)

                CoroutineScope(Dispatchers.IO).launch {
                    database.usuarios().delete(usuario)
                    ImagenController.deleteImage(this@UsuarioActvity, usuario.idUsuario.toLong())
                    this@UsuarioActvity.finish()
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when{
            requestCode == EDIT_ACTIVITY && resultCode == Activity.RESULT_OK->{
                imagen_det.setImageURI(data!!.data)
            }
        }
    }
}