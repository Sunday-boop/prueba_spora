package com.example.prueba_spora

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "Usuarios")
class Usuario (
    val nombre:String,
    val correo:String,
    val telefono:String,
    val edad:Int,
    val imagen:Int,
    @PrimaryKey(autoGenerate = true)
    var idUsuario: Int = 0
    ):Serializable