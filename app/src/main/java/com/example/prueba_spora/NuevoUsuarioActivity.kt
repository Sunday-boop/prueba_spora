package com.example.prueba_spora

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_nuevo_usuario.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NuevoUsuarioActivity : AppCompatActivity() {
    private  val SELECT_ACTIVITY = 50
    private var imageUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nuevo_usuario)

        var idUsuario: Int? = null
        if (intent.hasExtra("usuario")){
            val usuario = intent.extras?.getSerializable("usuario") as Usuario

            nombre_ins.setText(usuario.nombre)
            correo_ins.setText(usuario.correo)
            telefono_ins.setText(usuario.telefono)
            edad_ins.setText(usuario.edad.toString())
            idUsuario = usuario.idUsuario

            val imageUri = ImagenController.getImageUri(this, idUsuario.toLong())
            imagenSelect.setImageURI(imageUri)
        }

        val database = AppDatabase.getDatabase(this)

        guardar_btn.setOnClickListener{
            if (validar()) {
                val nombre = nombre_ins.text.toString()
                val correo = correo_ins.text.toString()
                val edad = edad_ins.text.toString().toInt()
                val telefono = telefono_ins.text.toString()

                val usuario =
                    Usuario(nombre, correo, telefono, edad, R.drawable.ic_launcher_background)

                if (idUsuario != null) {
                    CoroutineScope(Dispatchers.IO).launch {
                        usuario.idUsuario = idUsuario

                        database.usuarios().update(usuario)
                        imageUri?.let {
                            val intent = Intent()
                            intent.data = it
                            setResult(Activity.RESULT_OK, intent)
                            ImagenController.saveImage(
                                this@NuevoUsuarioActivity,
                                idUsuario.toLong(),
                                it
                            )
                        }

                        this@NuevoUsuarioActivity.finish()
                    }
                } else {
                    CoroutineScope(Dispatchers.IO).launch {
                        val id = database.usuarios().insertAll(usuario)[0]

                        imageUri?.let {
                            ImagenController.saveImage(this@NuevoUsuarioActivity, id, it)
                        }
                    }

                    var listaUsuarios = emptyArray<Usuario>()
                    val database = AppDatabase.getDatabase(this@NuevoUsuarioActivity)
                    database.usuarios().getAuth(correo)
                        .observe(this@NuevoUsuarioActivity, Observer {
                            listaUsuarios = it.toTypedArray()

                            if (listaUsuarios.isNotEmpty()) {
                                startActivity(
                                    Intent(
                                        this@NuevoUsuarioActivity,
                                        MostrarUsuariosActivity::class.java
                                    )
                                )
                            }
                        })
                }
            }
        }

        imagenSelect.setOnClickListener {
            ImagenController.selectFotoDeGaleria(this, 50)
        }
    }

    private fun validar():Boolean{
        var retorno:Boolean = true
        val nom = nombre_ins.text.toString()
        val cor = correo_ins.text.toString()
        val eda = edad_ins.text.toString()
        val tel = telefono_ins.text.toString()

        if (nom.isEmpty()){
            nombre_ins.setError("Este campo no puede estar vacio")
            retorno = false
        }
        if (cor.isEmpty()){
            correo_ins.setError("Este campo no puede estar vacio")
            retorno = false
        }
        if (tel.isEmpty()){
            telefono_ins.setError("Este campo no puede estar vacio")
            retorno = false
        }
        if (eda.isEmpty()){
            edad_ins.setError("Este campo no puede estar vacio")
            retorno = false
        }

        return retorno
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when{
            requestCode == SELECT_ACTIVITY && resultCode == Activity.RESULT_OK ->{
                imageUri = data!!.data
                imagenSelect.setImageURI(imageUri)
            }
        }
    }
}