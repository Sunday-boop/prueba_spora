package com.example.prueba_spora

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.item_usuario.view.*

class UsuarioAdapter(private val mContext: Context, private val listaUsuarios: Array<Usuario>):ArrayAdapter<Usuario>(mContext,0, listaUsuarios) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(mContext).inflate(R.layout.item_usuario, parent, false)
        val usuario = listaUsuarios[position]
        layout.nombre.text = usuario.nombre
        layout.correo.text = usuario.correo
        val imageUri =ImagenController.getImageUri(mContext,usuario.idUsuario.toLong())
        layout.imageView.setImageURI(imageUri)

        return layout
    }
}