package com.example.prueba_spora

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_mostrar_usuarios.*
import kotlinx.android.synthetic.main.activity_nuevo_usuario.*

class MainActivity : AppCompatActivity() {
    private lateinit var correo: Usuario
    private lateinit var correoLivedData: LiveData<Usuario>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val database = AppDatabase.getDatabase(this)
        correoLivedData = database.usuarios().getLast()
        if (correoLivedData.value != null){
            correoLivedData.observe(this, Observer {
                correo = it
                correo_login.setText("${correo.correo}")
            })
        }

        btnIngresar.setOnClickListener{
            val correo = correo_login.text.toString()

            var listaUsuarios = emptyArray<Usuario>()
            val database = AppDatabase.getDatabase(this)
            database.usuarios().getAuth(correo).observe(this, Observer {
                listaUsuarios = it.toTypedArray()

                if (listaUsuarios.isNotEmpty()) {
                    Toast.makeText(this, "login", Toast.LENGTH_LONG).show()
                    startActivity(Intent(this, MostrarUsuariosActivity::class.java))
                }else{
                    Toast.makeText(this, "No login", Toast.LENGTH_LONG).show()
                    startActivity(Intent(this, NuevoUsuarioActivity::class.java))
                }
            })
        }
    }
}