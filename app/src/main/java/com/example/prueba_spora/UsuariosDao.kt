package com.example.prueba_spora

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface UsuariosDao {
    @Query("SELECT * FROM Usuarios ORDER BY idUsuario DESC")
    fun getAll(): LiveData<List<Usuario>>

    @Query("SELECT MAX(idUsuario), nombre, correo, telefono, edad, imagen, idUsuario FROM Usuarios")
    fun getLast(): LiveData<Usuario>

    @Query("SELECT * FROM Usuarios WHERE idUsuario = :id")
    fun get(id:Int): LiveData<Usuario>

    @Query("SELECT * FROM Usuarios WHERE correo = :correo")
    fun getAuth(correo:String): LiveData<List<Usuario>>

    @Insert
    fun insertAll(vararg usuarios: Usuario): List<Long>

    @Update
    fun update(usuario: Usuario)

    @Delete
    fun delete(usuario: Usuario)
}