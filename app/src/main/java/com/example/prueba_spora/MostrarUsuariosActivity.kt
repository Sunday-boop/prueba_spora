package com.example.prueba_spora

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_mostrar_usuarios.*
import kotlinx.android.synthetic.main.item_usuario.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MostrarUsuariosActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mostrar_usuarios)


        var listaUsuarios = emptyArray<Usuario>()
        val database = AppDatabase.getDatabase(this)
        database.usuarios().getAll().observe(this, Observer {
            listaUsuarios = it.toTypedArray()

            if (listaUsuarios.isNotEmpty()) {
                val adapter = UsuarioAdapter(this, listaUsuarios)
                lista.adapter = adapter
            }else{
                finish()
                val intent = Intent(this, NuevoUsuarioActivity::class.java)
                startActivity(intent)
            }
        })

        lista.setOnItemClickListener{ parent, view, position, id->
            val intent = Intent(this, UsuarioActvity::class.java)
            intent.putExtra("id", listaUsuarios[position].idUsuario)
            startActivity(intent)
        }

        agregar_btn.setOnClickListener{
            val intent = Intent(this, NuevoUsuarioActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.salir, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.btn_salir ->{
                finish()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }

        return super.onOptionsItemSelected(item)
    }
}